from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm


# Create your views here.
@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": show_my_tasks,
    }
    return render(request, "show_my_tasks.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.assignee = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()

    context = {
        "form": form,
    }
    return render(request, "create_task.html", context)
